#-------------------------------------------------
#
# Project created by QtCreator 2015-11-05T17:46:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = metrika
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    functional.cpp

HEADERS  += mainwindow.h \
    functional.h

FORMS    += mainwindow.ui
