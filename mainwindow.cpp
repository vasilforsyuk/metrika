#include "mainwindow.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->functional = new MyFunc();
}
MainWindow::~MainWindow()
{
    delete ui;
    delete functional;
}
//количество строк кода
void MainWindow::on_SLOC_clicked()
{
    int num = this->functional->rowNumber(ui->plainTextEdit->toPlainText());
    ui->label->setText(QString::number(num+1));
}
// количество пустых строк
void MainWindow::on_EmptyLines_clicked()
{
    int empty_num = this->functional->emptyRowNumber(ui->plainTextEdit->toPlainText());
    ui->label_2->setText(QString::number(empty_num+1));
}
//количество комментариев
void MainWindow::on_Comments_clicked()
{
        QString str=ui->plainTextEdit->toPlainText();
        int comment_number = this->functional->commentNumber(str);
        ui->label_3->setText(QString::number(comment_number));
        int num = this->functional->rowNumber(str);
        ui->label_4->setText("("+QString::number(((float)comment_number)/(num+1)*100)+"%)");
}
//количество строк кода(кол-во строк не начинающихся с комментария - количество пустых строк)
void MainWindow::on_OnlyCode_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    int uncomment = this->functional->uncommentRowNumber(str);
    int empty = this->functional->emptyRowNumber(str);
    ui->label_5->setText(QString::number(uncomment-empty));
}
//словарь операндов
void MainWindow::on_VocOperandn2_clicked()
{
    int num = this->functional->operandVacebulary2(ui->plainTextEdit->toPlainText());
    ui->label_6->setText(QString::number(num));
}
//общее кол-во операндов
void MainWindow::on_AllOperandN2_clicked()
{
    int num = this->functional->allOperand(ui->plainTextEdit->toPlainText());
    ui->label_7->setText(QString::number(num));
}
//словарь операторов
void MainWindow::on_VocOperatn1_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    ui->label_8->setText(QString::number(this->functional->operandVacebulary(str)));
}
//общее кол-во операторов
void MainWindow::on_AllOperatN1_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    ui->label_9->setText(QString::number(this->functional->allOperand1(str)));
}
//цикломатическая сложность
void MainWindow::on_Cyclomatic_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    ui->label_10->setText(QString::number(this->functional->ciclic(str)));
}
//словарь программы
void MainWindow::on_Voc_clicked()
{
    QString str = ui->plainTextEdit->toPlainText();
    ui->label_11->setText(QString::number(this->functional->operandVacebulary(str)+this->functional->operandVacebulary2(str)-this->functional->rRr(str)));
}
//длина программы
void MainWindow::on_N_clicked()
{
    QString str = ui->plainTextEdit->toPlainText();
    int N = this->functional->allOperand(str) + this->functional->allOperand1(str) + 2;
    ui->label_12->setText(QString::number(N));
}
//объем программы
void MainWindow::on_V_clicked()
{
    QString str = ui->plainTextEdit->toPlainText();
    int N = this->functional->allOperand(str) + this->functional->allOperand1(str) + 2;
    int m = this->functional->rowNumber(str);
    float q=N*qLn(m)/qLn(2);
    ui->label_13->setText(QString::number(q));
}
//абсолютная сложность по Джилбу – количество условных операторов
void MainWindow::on_Cl_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    ui->label_14->setText(QString::number(str.count("if(")));
}
