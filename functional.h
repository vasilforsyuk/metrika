#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H
#include <QString>
#include <QStringList>
#include <algorithm>
class MyFunc
{
public:
    MyFunc();
    int rowNumber(QString str);
    int emptyRowNumber(QString str);
    int commentNumber(QString str);
    int uncommentRowNumber(QString str);
    int operandVacebulary(QString str);
    int operandVacebulary2(QString str);
    int operandNumber(QString str);
    int allOperand(QString str);
    int allOperand1(QString str);
    int ciclic(QString str);
    int rRr(QString str);
};

#endif // FUNCTIONAL_H
