#include "functional.h"
MyFunc::MyFunc()
{
}
int MyFunc::rowNumber(QString str)
{
    return str.count("\n");
}
int MyFunc::emptyRowNumber(QString str)
{
    return str.count("\n\n");
}
int MyFunc::commentNumber(QString str)
{
    int count = 0;
    int pos = 0;
    while (pos != -1)
    {
        pos = str.indexOf("//", pos);
        if (pos == -1)
            break;
        count++;
        pos = str.indexOf('\n', pos);
    }
    return count;
}
int MyFunc::uncommentRowNumber(QString str)
{
    int count = 0;
    int pos = 0;
    while (pos != -1)
    {
        int npos = str.indexOf("\n//", pos);
        if (npos != pos)
           count++;
        pos = str.indexOf('\n', pos+1);
    }
    return count;
}
int MyFunc::operandVacebulary(QString str)
{
    QStringList list = str.split(" ");
    QString types="+ - = += ++ -- * << >> < > != == || && &";
    QStringList Itypes;
    QStringList ress;
    list.removeDuplicates();
    Itypes=types.split(" ");
    QStringList::Iterator itt=Itypes.begin();
    for (int i=list.count()-1;i>=0;--i)
    {
        const QString&item=list[i];
        itt=Itypes.begin();
        while (itt!=Itypes.end())
        {
            if (item==*itt)
            {
                ress<<item;
            }
                ++itt;
            }
    }
    return (ress.count() + this->rRr(str));
}
int MyFunc::operandVacebulary2(QString str)
{
    QRegExp rx("([\\w\\']+)[\\s,.;]");
    QStringList list;
    int pos=0;
    while ((pos=rx.indexIn(str,pos))!=-1)
    {
        list<<rx.cap(1);
        pos+=rx.matchedLength();
    }
    list.removeDuplicates();
    QString types="int void string double float iterator List const array for while do goto";
    QStringList Itypes;
    Itypes=types.split(" ");
    QStringList::Iterator itt=Itypes.begin();
    for (int i=list.count()-1;i>=0;--i)
    {
        const QString&item=list[i];
        itt=Itypes.begin();
        while (itt!=Itypes.end())
        {
            if (item==*itt)
                list.removeAt(i);
            ++itt;
        }
        if ((item.toFloat()!=0))
        {
                list.removeAt(i);
        }
    }
    for (int i=list.count()-1;i>=0;--i)
    {
        const QString&item=list[i];
        if (item=="0")
        {
            list.removeAt(i);
        }
    }
    return list.count();
}
int MyFunc::allOperand(QString str)
{
    QRegExp rx("([\\w\\']+)[\\s,.;]");
    QStringList list;
    int pos=0;
    while ((pos=rx.indexIn(str,pos))!=-1)
    {
        list<<rx.cap(1);
        pos+=rx.matchedLength();
    }
    list.removeDuplicates();
    QString types="int void string double float iterator List const array for while do goto";
    QStringList Itypes;
    Itypes=types.split(" ");
    QStringList::Iterator itt=Itypes.begin();
    for (int i=list.count()-1;i>=0;--i)
    {
        const QString&item=list[i];
        itt=Itypes.begin();
        while (itt!=Itypes.end())
        {
            if (item==*itt)
                list.removeAt(i);
            ++itt;
        }
        if ((item.toFloat()!=0))
        {
            list.removeAt(i);
        }
    }
    for (int i=list.count()-1;i>=0;--i)
    {
        const QString&item=list[i];
        if (item=="0")
        {
            list.removeAt(i);
        }
    }
    return list.count();
}
int MyFunc::allOperand1(QString str)
{
    QStringList list=str.split(" ");
    QString types = "+ - = += ++ -- * << >> < > != == || && &";
    QStringList Itypes;
    QStringList ress;
    Itypes=types.split(" ");
    QStringList::Iterator itt=Itypes.begin();
    for (int i=list.count()-1; i>=0; --i)
    {
        const QString& item=list[i];
        itt=Itypes.begin();
        while (itt!=Itypes.end() )
        {
            if(item==*itt)
                ress<<item;
            ++itt;
        }
    }
    return ress.count() + this->rRr(str);
}
int MyFunc::ciclic(QString str)
{
    QStringList list=str.split(QRegExp("(for|while)"),QString::SkipEmptyParts);
    QStringList::Iterator it=list.begin();
    int s=0;
    for (int i=list.count()-1; i>=0; --i)
    {
        const QString& item=list[i];
        if ((item.count("{")>=item.count("}"))&&(item.count("}")!=0))
            s=s+1;
    }
    return s+str.count("if(");
}
int MyFunc::rRr(QString str)
{
    int r=0;
    QStringList lst=str.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"), QString::SkipEmptyParts);
    QStringList::iterator t=lst.begin();
    for (int i=lst.count()-1;i>=0;--i)
    {
        const QString&item=lst[i];
        r=r+item.count(")\n");
    }
    return r;
}
