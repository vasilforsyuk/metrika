#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <cmath>
#include <QtCore/qmath.h>
#include "ui_mainwindow.h"
#include "functional.h"
namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_SLOC_clicked();
    void on_EmptyLines_clicked();
    void on_Comments_clicked();
    void on_OnlyCode_clicked();
    void on_VocOperandn2_clicked();
    void on_AllOperandN2_clicked();
    void on_VocOperatn1_clicked();
    void on_AllOperatN1_clicked();
    void on_Cyclomatic_clicked();
    void on_Voc_clicked();
    void on_N_clicked();
    void on_V_clicked();
    void on_Cl_clicked();
private:
    Ui::MainWindow *ui;
    MyFunc * functional;
};
#endif // MAINWINDOW_H
